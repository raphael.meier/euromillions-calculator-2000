#include <iostream>
#include <string>

#include "core/calculator.h"
#include "core/combination.h"
#include "utils/configuration.h"
#include "utils/error.h"

void usage(const std::string& programme_name) {
  std::cout << programme_name << " [b1] [b2] [b3] [b4] [b5] [s1] [s2]" << std::endl;
}

int main(int argc, char* argv[]) {
  if (argc != 8) {
    usage(argv[0]);
    return EXIT_FAILURE;
  }

  ec2000::utils::Configuration::Instance()->SetLogger();

  std::string configuration_file_path;
  if (EC2000_SUCCESS(ec2000::utils::Configuration::FindConfigurationFile(
          "config.json", &configuration_file_path))) {
    ec2000::utils::Configuration::Instance()->LoadConfigurationFile(configuration_file_path);
  } else {
    std::cout << "Can not find configuration file" << std::endl;
  }

  ec2000::core::Combination combination;
  try {
    combination = ec2000::core::Combination{
        static_cast<uint16_t>(std::stoi(argv[1])), static_cast<uint16_t>(std::stoi(argv[2])),
        static_cast<uint16_t>(std::stoi(argv[3])), static_cast<uint16_t>(std::stoi(argv[4])),
        static_cast<uint16_t>(std::stoi(argv[5])), static_cast<uint16_t>(std::stoi(argv[6])),
        static_cast<uint16_t>(std::stoi(argv[7])),
    };
  } catch (const std::invalid_argument& e) {
    std::cout << "Error with input combination! Error on " << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  int occurencies = 0;
  ec2000::core::Calculator calculator;
  calculator.GetCombinationOccurencies(combination, &occurencies);

  std::cout << "This combination " << combination.ToString() << " has been the winning combination "
            << occurencies << " times on " << calculator.number_of_draws() << " draws" << std::endl;
  std::cout << "There is " << calculator.unparseable_line() << " CSV lines which can not be parsed"
            << std::endl;
  return EXIT_SUCCESS;
}
