#include "core/combination.h"

#include <algorithm>
#include <vector>

#include <spdlog/spdlog.h>

#include "utils/error.h"

namespace ec2000 {
namespace core {

/*
 * Static methods
 */

/*
 * Public methods
 */

std::vector<uint16_t> Combination::OrderedVector() const {
  std::vector<uint16_t> ordered_vector;
  std::vector<uint16_t> vector = Vector();

  for (uint16_t i = 5; i > 0; --i) {
    std::vector<uint16_t>::iterator minimal_value_iterator =
        std::min_element(vector.begin(), vector.begin() + i);
    ordered_vector.emplace_back(vector[minimal_value_iterator - vector.begin()]);
    vector.erase(minimal_value_iterator);
  }

  if (vector[0] < vector[1]) {
    ordered_vector.emplace_back(vector[0]);
    ordered_vector.emplace_back(vector[1]);
  } else {
    ordered_vector.emplace_back(vector[1]);
    ordered_vector.emplace_back(vector[0]);
  }

  spdlog::trace("Ordered vector is [{},{},{},{},{},{},{}]", ordered_vector[0], ordered_vector[1],
                ordered_vector[2], ordered_vector[3], ordered_vector[4], ordered_vector[5],
                ordered_vector[6]);

  return ordered_vector;
}

}  // namespace core
}  // namespace ec2000
