#include "core/draw.h"

#include <ctime>
#include <iomanip>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include <spdlog/spdlog.h>

#include "core/combination.h"
#include "utils/error.h"

namespace ec2000 {
namespace core {

/*
 * Static methods
 */

utils::EC2000Error Draw::ParseCSVLineToDraw(std::string_view CSV_line, Draw* draw) {
  std::vector<std::string> explode_CSV_line;

  u_int last_delimiter_position = 0;
  char delimiter = ';';

  for (u_int i = 0; i < CSV_line.size(); ++i) {
    if (CSV_line[i] == delimiter) {
      explode_CSV_line.emplace_back(
          CSV_line.substr(last_delimiter_position + 1, i - last_delimiter_position - 1));
      last_delimiter_position = i;
    }
  }

  if (explode_CSV_line.size() < 13) {
    spdlog::error("Informations are missing inside the CSV line: \"{}\"", CSV_line);
    return utils::EC2000Error::kCSVMalformed;
  }

  int column_increment = 0;
  std::tm draw_date;
  if (EC2000_FAILURE(GetDrawDateFromString(explode_CSV_line[2], &draw_date))) {
    spdlog::error("Can not parse date {}", explode_CSV_line[2]);
  } else {
    spdlog::trace("Date parsed {}", explode_CSV_line[3]);
  }

  // The most recent results files has one more column before the forclusion date. So if we can't
  // parse this column, we increment the column number
  std::tm temp_date;
  if (EC2000_FAILURE(GetDrawDateFromString(explode_CSV_line[3], &temp_date))) {
    ++column_increment;
  }

  try {
    Combination combination = Combination{
        static_cast<uint16_t>(std::stoi(explode_CSV_line[4 + column_increment])),
        static_cast<uint16_t>(std::stoi(explode_CSV_line[5 + column_increment])),
        static_cast<uint16_t>(std::stoi(explode_CSV_line[6 + column_increment])),
        static_cast<uint16_t>(std::stoi(explode_CSV_line[7 + column_increment])),
        static_cast<uint16_t>(std::stoi(explode_CSV_line[8 + column_increment])),
        static_cast<uint16_t>(std::stoi(explode_CSV_line[9 + column_increment])),
        static_cast<uint16_t>(std::stoi(explode_CSV_line[10 + column_increment])),
    };
    *draw = Draw{draw_date, combination, explode_CSV_line[11 + column_increment],
                 explode_CSV_line[12 + column_increment]};
  } catch (const std::invalid_argument& e) {
    spdlog::error("Error \"{}\" when parsing CSV line: \"{}\"", e.what(), CSV_line);
    return utils::EC2000Error::kCSVMalformed;
  } catch (const std::out_of_range& e) {
    spdlog::error("Error \"{}\" when parsing CSV line: \"{}\"", e.what(), CSV_line);
    return utils::EC2000Error::kCSVMalformed;
  }

  spdlog::debug("Line parsing succeed");
  return utils::EC2000Error::kSuccess;
}

utils::EC2000Error Draw::GetDrawDateFromString(const std::string& date_string, std::tm* date) {
  if (date_string.length() != 8 && date_string.length() != 10) {
    spdlog::debug("Can not parse date \"{}\", length missmatch", date_string);
    return utils::EC2000Error::kFailure;
  }
  std::istringstream ss{date_string};
  ss >> std::get_time(date, "%d/%m/%Y");
  if (ss.fail()) {
    spdlog::debug("Can not parse date first format: \"{}\"", date_string);
    ss = std::istringstream{date_string};
    ss >> std::get_time(date, "%Y%m%d");
    if (ss.fail()) {
      spdlog::debug("Can not parse date second format: \"{}\"", date_string);
      return utils::EC2000Error::kFailure;
    }
  }
  return utils::EC2000Error::kSuccess;
}

/*
 * Public methods
 */

bool Draw::IsWinningCombination(const Combination& combination) {
  return combination_ == combination;
}

bool operator==(const Draw& left_draw, const Draw& right_draw) {
  if (!left_draw.ordered_winning_balls().empty() && !left_draw.ordered_winning_stars().empty() &&
      !right_draw.ordered_winning_balls().empty() && !right_draw.ordered_winning_stars().empty()) {
    return (left_draw.ordered_winning_balls() + left_draw.ordered_winning_stars()) ==
           (right_draw.ordered_winning_balls() + right_draw.ordered_winning_stars());
  }
  return left_draw.combination() == right_draw.combination();
}

std::string Draw::ToString() {
  std::string combination_string;
  if (!ordered_winning_balls_.empty() && !ordered_winning_stars_.empty()) {
    combination_string = ordered_winning_balls_ + ordered_winning_stars_;
  } else {
    combination_string = combination_.ToString();
  }
  std::ostringstream oss;
  oss << std::put_time(&date_, "%d/%m/%Y");
  return oss.str() + ": " + combination_string;
}

}  // namespace core
}  // namespace ec2000
