#ifndef SRC_LIB_CORE_DRAW_H_
#define SRC_LIB_CORE_DRAW_H_

#include <ctime>
#include <string>
#include <string_view>

#include "core/combination.h"
#include "utils/error.h"

namespace ec2000 {
namespace core {

class Draw {
 public:
  Draw() {}
  Draw(const std::tm& date, const Combination& combination)
      : date_(date), combination_(combination) {}
  Draw(const std::tm& date, const Combination& combination,
       const std::string& ordered_winning_balls, const std::string& ordered_winning_stars)
      : date_(date),
        combination_(combination),
        ordered_winning_balls_(ordered_winning_balls),
        ordered_winning_stars_(ordered_winning_stars) {}

  static utils::EC2000Error ParseCSVLineToDraw(std::string_view CSV_line, Draw* draw);
  static utils::EC2000Error GetDrawDateFromString(const std::string& date_string, std::tm* date);

  // Getter
  Combination combination() const { return combination_; }
  std::tm date() const { return date_; }
  std::string ordered_winning_balls() const { return ordered_winning_balls_; }
  std::string ordered_winning_stars() const { return ordered_winning_stars_; }

  friend bool operator==(const Draw& left_draw, const Draw& right_draw);
  friend bool operator!=(const Draw& left_draw, const Draw& right_draw) {
    return !(left_draw == right_draw);
  }

  bool IsWinningCombination(const Combination& combination);
  std::string ToString();

 private:
  Combination combination_;
  std::tm date_;
  std::string ordered_winning_balls_;
  std::string ordered_winning_stars_;
};

}  // namespace core
}  // namespace ec2000

#endif  // SRC_LIB_CORE_DRAW_H_
