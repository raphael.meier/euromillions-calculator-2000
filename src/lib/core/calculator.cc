#include "core/calculator.h"

#include <filesystem>
#include <fstream>
#include <string>

#include <spdlog/spdlog.h>

#include "core/combination.h"
#include "core/draw.h"
#include "utils/configuration.h"
#include "utils/error.h"

namespace ec2000 {
namespace core {

/*
 * Public methods
 */
utils::EC2000Error Calculator::GetCombinationOccurencies(const Combination& combination,
                                                         int* occurencies) {
  if (!std::filesystem::exists(utils::Configuration::Instance()->resources_directory())) {
    spdlog::error("Can not find resources directory at {}",
                  utils::Configuration::Instance()->resources_directory());
    return utils::EC2000Error::kFileNotFound;
  }

  // Reseting calculator parameters
  number_of_draws_ = 0;
  unparseable_line_ = 0;

  *occurencies = 0;
  for (const std::filesystem::directory_entry& entry : std::filesystem::directory_iterator(
           utils::Configuration::Instance()->resources_directory())) {
    if (std::filesystem::is_regular_file(entry) && entry.path().extension().string() == ".csv") {
      std::string entry_path_string = entry.path().string();
      std::ifstream csv_file{entry_path_string};

      if (!csv_file.is_open()) {
        spdlog::warn("Can not open file {}", entry_path_string);
        break;
      }

      spdlog::debug("Reading file {}", entry_path_string);

      std::string csv_line;
      while (std::getline(csv_file, csv_line)) {
        Draw draw;
        if (EC2000_FAILURE(Draw::ParseCSVLineToDraw(csv_line, &draw))) {
          spdlog::warn("Line \"{}...\" is skipped", csv_line.substr(0, 20));
          ++unparseable_line_;
        } else {
          if (draw.IsWinningCombination(combination)) {
            spdlog::info("Found a winning draw {}", draw.ToString());
            ++(*occurencies);
          } else {
            spdlog::info("Draw {} is not similar", draw.ToString());
          }
          ++number_of_draws_;
        }
      }
    }
  }

  return utils::EC2000Error::kSuccess;
}

}  // namespace core
}  // namespace ec2000
