#ifndef SRC_LIB_CORE_COMBINATION_H_
#define SRC_LIB_CORE_COMBINATION_H_

#include <string>
#include <string_view>
#include <vector>

#include "utils/error.h"

namespace ec2000 {
namespace core {

class Combination {
 public:
  Combination() {}
  Combination(uint16_t ball_1, uint16_t ball_2, uint16_t ball_3, uint16_t ball_4, uint16_t ball_5,
              uint16_t star_1, uint16_t star_2)
      : ball_1_(ball_1),
        ball_2_(ball_2),
        ball_3_(ball_3),
        ball_4_(ball_4),
        ball_5_(ball_5),
        star_1_(star_1),
        star_2_(star_2) {}

  // TODO(Raphael): Do this method
  // static utils::EC2000Error ParseFromString(std::string_view combinaison_string, std::string_view
  // delimiter, Combination* combination)

  std::vector<uint16_t> Vector() const {
    return std::vector<uint16_t>{ball_1_, ball_2_, ball_3_, ball_4_, ball_5_, star_1_, star_2_};
  }
  std::vector<uint16_t> OrderedVector() const;

  friend bool operator==(const Combination& left_combination,
                         const Combination& right_combination) {
    return left_combination.OrderedVector() == right_combination.OrderedVector();
  }
  friend bool operator!=(const Combination& left_combination,
                         const Combination& right_combination) {
    return !(left_combination == right_combination);
  }

  std::string ToString() {
    return "-" + std::to_string(ball_1_) + "-" + std::to_string(ball_2_) + "-" +
           std::to_string(ball_3_) + "-" + std::to_string(ball_4_) + "-" + std::to_string(ball_5_) +
           "--" + std::to_string(star_1_) + "-" + std::to_string(star_2_) + "-";
  }

 private:
  uint16_t ball_1_;
  uint16_t ball_2_;
  uint16_t ball_3_;
  uint16_t ball_4_;
  uint16_t ball_5_;
  uint16_t star_1_;
  uint16_t star_2_;
};

}  // namespace core
}  // namespace ec2000

#endif  // SRC_LIB_CORE_COMBINATION_H_
