#ifndef SRC_LIB_CORE_CALCULATOR_H_
#define SRC_LIB_CORE_CALCULATOR_H_

#include "core/combination.h"
#include "utils/error.h"

namespace ec2000 {
namespace core {

class Calculator {
 public:
  utils::EC2000Error GetCombinationOccurencies(const Combination& combination, int* occurencies);

  // Getter
  int number_of_draws() { return number_of_draws_; }
  int unparseable_line() { return unparseable_line_; }

 private:
  int number_of_draws_ = 0;
  int unparseable_line_ = 0;
};

}  // namespace core
}  // namespace ec2000

#endif  // SRC_LIB_CORE_CALCULATOR_H_
