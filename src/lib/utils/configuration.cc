#include "utils/configuration.h"

#include <filesystem>
#include <fstream>
#include <string>

#include <nlohmann/json.hpp>

#include <spdlog/spdlog.h>

#include "utils/error.h"

namespace ec2000 {
namespace utils {

utils::EC2000Error Configuration::FindConfigurationFile(const std::string& configuration_filename,
                                                        std::string* configuration_file_path) {
  // The highest priority file on top
  std::string locations[] = {"/etc/euromillions-calculator-2000/", "./config", "./"};

  for (const std::string& location : locations) {
    std::string configuration_full_path = location + "/" + configuration_filename;
    if (std::filesystem::exists(configuration_full_path)) {
      *configuration_file_path = configuration_full_path;
      spdlog::info("Configuration file found at {}", *configuration_file_path);
      return utils::EC2000Error::kSuccess;
    }
  }
  spdlog::warn("No configuration file found");
  return utils::EC2000Error::kFileNotFound;
}

utils::EC2000Error Configuration::LoadConfigurationFile(
    const std::string& configuration_file_path) {
  std::ifstream configuration_file{configuration_file_path};
  if (!configuration_file) {
    spdlog::warn("Can not open configuration file at {} with error: \"{}\"",
                 configuration_file_path, strerror(errno));
    return utils::EC2000Error::kCanNotOpenFile;
  }

  try {
    nlohmann::json configuration_json = nlohmann::json::parse(configuration_file);

    if (configuration_json.contains("resources_directory")) {
      resources_directory_ = configuration_json["resources_directory"];
    }
  } catch (const nlohmann::json::exception& e) {
    spdlog::warn("Error when parsing configuration file {} with error: \"{}\"",
                 configuration_file_path, e.what());
    return utils::EC2000Error::kJsonError;
  }

  spdlog::info("Configuration is loaded with file {}", configuration_file_path);
  return utils::EC2000Error::kSuccess;
}

utils::EC2000Error Configuration::SetLogger() {
  // spdlog::set_pattern("[%H:%M:%S %z] [%n] [%^---%L---%$] [thread %t] %v");
  spdlog::set_level(spdlog::level::info);
  spdlog::debug("Logger setted");
  return utils::EC2000Error::kSuccess;
}

}  // namespace utils
}  // namespace ec2000
