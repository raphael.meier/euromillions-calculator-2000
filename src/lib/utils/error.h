#ifndef SRC_LIB_UTILS_ERROR_H_
#define SRC_LIB_UTILS_ERROR_H_

namespace ec2000 {
namespace utils {

enum EC2000Error { kSuccess, kFailure, kCanNotOpenFile, kFileNotFound, kCSVMalformed, kJsonError };

#define EC2000_SUCCESS(x) x == ec2000::utils::EC2000Error::kSuccess
#define EC2000_FAILURE(x) x != ec2000::utils::EC2000Error::kSuccess

}  // namespace utils
}  // namespace ec2000

#endif  // SRC_LIB_UTILS_ERROR_H_
