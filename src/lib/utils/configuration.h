#ifndef SRC_LIB_UTILS_CONFIGURATION_H_
#define SRC_LIB_UTILS_CONFIGURATION_H_

#include <string>

#include "utils/error.h"

namespace ec2000 {
namespace utils {

class Configuration {
 public:
  /* Scott Meyers singleton */
  static Configuration* Instance() {
    static Configuration instance;
    return &instance;
  }
  Configuration(const Configuration& configuration) = delete;
  void operator=(const Configuration& Configuration) = delete;

  static utils::EC2000Error FindConfigurationFile(const std::string& configuration_filename,
                                                  std::string* configuration_file_path);

  // Getter
  std::string resources_directory() { return resources_directory_; }

  utils::EC2000Error LoadConfigurationFile(const std::string& configuration_file_path);

  utils::EC2000Error SetLogger();

 private:
  Configuration() {}

  std::string resources_directory_ = "./resources";
};

}  // namespace utils
}  // namespace ec2000

#endif  // SRC_LIB_UTILS_CONFIGURATION_H_
