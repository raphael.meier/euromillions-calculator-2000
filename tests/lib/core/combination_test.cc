#include "core/combination.h"

#include <vector>

#include <gtest/gtest.h>

#include "utils/error.h"

class CombinationTest : public ::testing::Test {
 protected:
  ec2000::core::Combination* combination;
  virtual void SetUp() { combination = new ec2000::core::Combination{2, 1, 3, 4, 5, 2, 1}; }

  virtual void TearDown() { delete combination; }
};

TEST_F(CombinationTest, VectorTest) {
  std::vector<uint16_t> assertion_vector{2, 1, 3, 4, 5, 2, 1};
  ASSERT_EQ(combination->Vector(), assertion_vector);
}

TEST_F(CombinationTest, OrderedVectorTest) {
  std::vector<uint16_t> assertion_vector{1, 2, 3, 4, 5, 1, 2};
  ASSERT_EQ(combination->OrderedVector(), assertion_vector);
}

TEST_F(CombinationTest, ToStringTest) { ASSERT_EQ(combination->ToString(), "-2-1-3-4-5--2-1-"); }
