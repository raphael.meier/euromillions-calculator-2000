#include "core/draw.h"

#include <ctime>
#include <vector>

#include <gtest/gtest.h>

#include "core/combination.h"
#include "utils/error.h"

class DrawTest : public ::testing::Test {
 protected:
  ec2000::core::Draw* draw;
  virtual void SetUp() {
    std::tm date;
    ec2000::core::Combination combination{2, 1, 5, 3, 4, 2, 1};
    draw = new ec2000::core::Draw{date, combination};
  }

  virtual void TearDown() { delete draw; }
};

TEST_F(DrawTest, IsWinningCombinationSuccessTest) {
  ec2000::core::Combination combination{1, 2, 4, 3, 5, 1, 2};
  ASSERT_TRUE(draw->IsWinningCombination(combination));
}

TEST_F(DrawTest, IsWinningCombinationFailureTest) {
  ec2000::core::Combination combination{1, 8, 4, 3, 5, 1, 2};
  ASSERT_TRUE(!draw->IsWinningCombination(combination));
}

TEST_F(DrawTest, OperatorEqualSuccessTest) {
  std::tm date;
  ec2000::core::Combination combination{1, 3, 4, 2, 5, 2, 1};
  ec2000::core::Draw other_draw{date, combination};
  ASSERT_TRUE(*draw == other_draw);
  ASSERT_TRUE(other_draw == *draw);
}

TEST_F(DrawTest, OperatorEqualFailureTest) {
  std::tm date;
  ec2000::core::Combination combination{42, 3, 4, 2, 5, 2, 1};
  ec2000::core::Draw other_draw{date, combination};
  ASSERT_FALSE(*draw == other_draw);
  ASSERT_FALSE(other_draw == *draw);
}

/* Static methods tests */

TEST(DrawStaticFunctionTest, GetDrawDateFromStringSuccessTest) {
  std::tm date;

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("19700201", &date),
            ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(date.tm_year, 70);
  ASSERT_EQ(date.tm_mon, 1);
  ASSERT_EQ(date.tm_mday, 1);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("25001030", &date),
            ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(date.tm_year, 600);
  ASSERT_EQ(date.tm_mon, 9);
  ASSERT_EQ(date.tm_mday, 30);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("01/02/1970", &date),
            ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(date.tm_year, 70);
  ASSERT_EQ(date.tm_mon, 1);
  ASSERT_EQ(date.tm_mday, 1);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("30/10/2500", &date),
            ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(date.tm_year, 600);
  ASSERT_EQ(date.tm_mon, 9);
  ASSERT_EQ(date.tm_mday, 30);
}

TEST(DrawStaticFunctionTest, GetDrawDateFromStringWeirdStringErrorTest) {
  std::tm date;

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("not_a_date", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("197021", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("197002010", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("19700a201", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("1/2/1970", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("01/02/19780", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("01/02/197b0", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("01/02/1970/", &date),
            ec2000::utils::EC2000Error::kFailure);
}

TEST(DrawStaticFunctionTest, GetDrawDateFromStringIllegalDateErrorTest) {
  std::tm date;

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("100002010", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("01151970", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("36/02/1970", &date),
            ec2000::utils::EC2000Error::kFailure);

  ASSERT_EQ(ec2000::core::Draw::GetDrawDateFromString("01/222/1970", &date),
            ec2000::utils::EC2000Error::kFailure);
}

TEST(DrawStaticFunctionTest, ParseCSVLineToDrawMinimalLineSuccessTest) {
  ec2000::core::Draw draw;
  std::vector<uint16_t> combination_vector;

  ASSERT_EQ(
      ec2000::core::Draw::ParseCSVLineToDraw("20200096;MARDI   "
                                             ";01/12/2020;10;01/04/"
                                             "2021;29;49;20;14;47;12;4;-14-20-29-47-49-;-4-12-;",
                                             &draw),
      ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(draw.date().tm_year, 120);
  ASSERT_EQ(draw.date().tm_mon, 11);
  ASSERT_EQ(draw.date().tm_mday, 1);
  ASSERT_EQ(draw.ordered_winning_balls(), "-14-20-29-47-49-");
  ASSERT_EQ(draw.ordered_winning_stars(), "-4-12-");
  combination_vector = std::vector<uint16_t>{14, 20, 29, 47, 49, 4, 12};
  ASSERT_EQ(draw.combination().OrderedVector(), combination_vector);
  ASSERT_EQ(draw.combination().ToString(), "-29-49-20-14-47--12-4-");
}

TEST(DrawStaticFunctionTest, ParseCSVLineToDrawRealLineSuccessTest) {
  ec2000::core::Draw draw;
  std::vector<uint16_t> combination_vector;

  ASSERT_EQ(ec2000::core::Draw::ParseCSVLineToDraw(
                "20200096;MARDI   "
                ";01/12/2020;10;01/04/"
                "2021;29;49;20;14;47;12;4;-14-20-29-47-49-;-4-12-;0;0;0;0;0;328905,6;1;0;38435,3;"
                "10;0;1355,2;144;0;161,1;319;0;75,6;378;0;49,9;4698;0;18,6;7382;0;13,7;18544;0;10,"
                "5;26020;0;8,6;115063;0;6,4;291381;0;4,1;0;0;3;833,1;46;30,1;122;11,3;1733;2;2620;"
                "3,5;9557;4,5;15848;12,7;40468;2,8;382566;2,6;FV 079 1121;;",
                &draw),
            ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(draw.date().tm_year, 120);
  ASSERT_EQ(draw.date().tm_mon, 11);
  ASSERT_EQ(draw.date().tm_mday, 1);
  ASSERT_EQ(draw.ordered_winning_balls(), "-14-20-29-47-49-");
  ASSERT_EQ(draw.ordered_winning_stars(), "-4-12-");
  combination_vector = std::vector<uint16_t>{14, 20, 29, 47, 49, 4, 12};
  ASSERT_EQ(draw.combination().OrderedVector(), combination_vector);
  ASSERT_EQ(draw.combination().ToString(), "-29-49-20-14-47--12-4-");

  ASSERT_EQ(ec2000::core::Draw::ParseCSVLineToDraw(
                "2011018;VE;20110506;20110706;28;16;20;22;11;4;9;-11-16-20-22-28-;-4-9-;0;1;"
                "28772008;6;14;227484;8;19;47567,7;40;149;4332,6;573;2244;191,7;898;3810;79;1545;"
                "6446;66,7;21482;93092;23,5;19781;84572;22,3;34626;152321;13,2;90180;410991;10,5;"
                "266603;1191381;8,6;1 037 960;eur;",
                &draw),
            ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(draw.date().tm_year, 111);
  ASSERT_EQ(draw.date().tm_mon, 4);
  ASSERT_EQ(draw.date().tm_mday, 6);
  ASSERT_EQ(draw.ordered_winning_balls(), "-11-16-20-22-28-");
  ASSERT_EQ(draw.ordered_winning_stars(), "-4-9-");
  combination_vector = std::vector<uint16_t>{11, 16, 20, 22, 28, 4, 9};
  ASSERT_EQ(draw.combination().OrderedVector(), combination_vector);
  ASSERT_EQ(draw.combination().ToString(), "-28-16-20-22-11--4-9-");

  ASSERT_EQ(
      ec2000::core::Draw::ParseCSVLineToDraw(
          "20200077;VENDREDI;25/09/2020;Super Jackpot et tirages "
          "suivants;25/11/"
          "2020;34;45;19;23;37;1;7;-19-23-34-37-45-;-1-7-;0;1;130000000;0;5;247047,8;5;25;11547,8;"
          "16;78;1152,8;374;1590;104,1;812;3162;55,3;801;3694;33,3;10810;44420;13,8;14416;63635;10,"
          "7;30469;142425;8,9;59230;237240;6,5;195280;867562;5,6;412900;1921549;4;0;0;5;689,5;125;"
          "15,3;222;8,6;3482;1,3;4561;2,8;19268;3,1;34308;8,1;63749;2,4;601772;2,3;GB 600 8763;;",
          &draw),
      ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(draw.date().tm_year, 120);
  ASSERT_EQ(draw.date().tm_mon, 8);
  ASSERT_EQ(draw.date().tm_mday, 25);
  ASSERT_EQ(draw.ordered_winning_balls(), "-19-23-34-37-45-");
  ASSERT_EQ(draw.ordered_winning_stars(), "-1-7-");
  combination_vector = std::vector<uint16_t>{19, 23, 34, 37, 45, 1, 7};
  ASSERT_EQ(draw.combination().OrderedVector(), combination_vector);
  ASSERT_EQ(draw.combination().ToString(), "-34-45-19-23-37--1-7-");
}

TEST(DrawStaticFunctionTest, ParseCSVLineToDrawEmptyLineErrorTest) {
  ec2000::core::Draw draw;
  ASSERT_EQ(ec2000::core::Draw::ParseCSVLineToDraw("", &draw),
            ec2000::utils::EC2000Error::kCSVMalformed);
}

TEST(DrawStaticFunctionTest, ParseCSVLineToDrawLineWithCharacterInsteadOfNumberErrorTest) {
  ec2000::core::Draw draw;
  ASSERT_EQ(ec2000::core::Draw::ParseCSVLineToDraw(
                "2011018;VE;20110506;20110706;a;16;20;22;11;4;9;-11-16-20-22-28-;-4-9-;", &draw),
            ec2000::utils::EC2000Error::kCSVMalformed);
}
