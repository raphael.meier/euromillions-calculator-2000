#include "utils/configuration.h"

#include <string>

#include <gtest/gtest.h>

#include "utils/error.h"

TEST(ConfigurationTest, FindConfigurationFileWrongFileNameTest) {
  std::string configuration_file_path;
  ASSERT_EQ(ec2000::utils::Configuration::Instance()->FindConfigurationFile(
                "NotAConfigurationName", &configuration_file_path),
            ec2000::utils::EC2000Error::kFileNotFound);
}

TEST(ConfigurationTest, FindConfigurationFileSuccessTest) {
  std::string configuration_file_path;
  ASSERT_EQ(ec2000::utils::Configuration::Instance()->FindConfigurationFile(
                "config.json", &configuration_file_path),
            ec2000::utils::EC2000Error::kSuccess);
}

TEST(ConfigurationTest, LoadConfigurationFileNonExistentFileTest) {
  ASSERT_EQ(ec2000::utils::Configuration::Instance()->LoadConfigurationFile("non_existent_file"),
            ec2000::utils::EC2000Error::kCanNotOpenFile);
}

TEST(ConfigurationTest, LoadConfigurationFileBadFileTest) {
  ASSERT_EQ(ec2000::utils::Configuration::Instance()->LoadConfigurationFile(
                "./tests_resources/bad_config.json.test"),
            ec2000::utils::EC2000Error::kJsonError);
}

TEST(ConfigurationTest, LoadConfigurationFileSuccessTest) {
  ASSERT_EQ(ec2000::utils::Configuration::Instance()->LoadConfigurationFile(
                "./tests_resources/config.json.test"),
            ec2000::utils::EC2000Error::kSuccess);
  ASSERT_EQ(ec2000::utils::Configuration::Instance()->resources_directory(), "correct_location");
}
