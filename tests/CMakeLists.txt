# Copy tests resources directory inside the binaries directory 
set(TESTS_RESOURCES_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/tests_resources)
file(COPY ${TESTS_RESOURCES_DIRECTORY} DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

set(TESTS_SOURCES
  lib/core/combination_test.cc
  lib/core/draw_test.cc
  
  lib/utils/configuration_test.cc
)

add_executable(ec_2000_unit_tests tests.cc ${TESTS_SOURCES})
target_link_libraries(ec_2000_unit_tests PRIVATE ec2000)
target_link_libraries(ec_2000_unit_tests PRIVATE ${GTEST_LIBRARIES} pthread)