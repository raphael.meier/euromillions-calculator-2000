[![pipeline status](https://gitlab.com/raphael.meier/euromillions-calculator-2000/badges/master/pipeline.svg)](https://gitlab.com/raphael.meier/euromillions-calculator-2000/-/commits/master)

# EuromillionsCalculator2000

This project help you to know if the Euromillions combination you give as an input has already won.
Disclaimer: it will not help you to win (probabilities don't work this way) but if you are like me you don't want to play a number that have already won. Otherwise I would always play the combination -1-2-3-4-5--6-7-.

This project can also be used as a good base for a C++ project.

## Resources
The first step is to acquire all the draws history. The files containing them are availables at [https://www.fdj.fr/jeux-de-tirage/euromillions-my-million/resultats] page bottom. You will download all the `.zip` files and extract them inside the `./resources` directory.

## Build
```bash
mkdir build
cd build
cmake ..
make
```

## Use
```bash
cd bin
./euromillions-calculator-2000 1 2 3 4 5 1 2
```

## Docker
This project can be run in dockers.

There is two Dockerfiles inside the `./docker` directory. `Dockerfile-builder` to build and `Dockerfile-runner` (more lightweight) to run the program.

To build the docker you can do:
```bash
./docker/build_runner.sh
```

## Dependencies
You can have a look inside the `./docker/Dockerfile-builder` to know the project dependencies.

## Developer tips
formater: clang-format -i $(find -path ./src/third_party -prune -false -o -name "*.cc" -o -name "*.h")
linter: cpplint  $(find -path ./src/third_party -prune -false -o -name "*.cc" -o -name "*.h")