#!/bin/bash

docker_builder_tag="euromillions-calculator-2000-builder"
docker_runner_tag="euromillions-calculator-2000"
version=$(cat version)

echo "BUILDING DOCKER BUILDER ..."
docker build -f docker/Dockerfile-builder -t $docker_builder_tag:$version .
if (( $? == 0 )); then
  echo "DOCKER BUILDER BUILT AS $docker_builder_tag:$version"
else
  echo "ERROR WHEN BUILDING DOCKER BUILDER"
  exit 1
fi

echo "COPYING BINARY FROM BUILDER ..."
mkdir -p bin
id=$(docker create $docker_builder_tag:$version)
docker cp $id:/euromillions-calculator-2000/bin/euromillions_calculator_2000 ./bin/euromillions_calculator_2000
if (( $? == 0 )); then
  echo "BINARY COPIED"
else
  echo "ERROR WHEN COPYING BINARY FROM DOCKER BUILDER"
  exit 1
fi

echo "BUILDING DOCKER RUNNER ..."
docker build -f docker/Dockerfile-runner -t $docker_runner_tag:$version .
if (( $? == 0 )); then
  echo "DOCKER RUNNER BUILT AS $docker_runner_tag:$version"
else
  echo "ERROR WHEN BUILDING DOCKER RUNNER"
  exit 1
fi
